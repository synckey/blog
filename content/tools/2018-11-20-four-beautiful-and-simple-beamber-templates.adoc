= Four Beautiful and Simple Beamer Templates
:docdate: 2018-11-20
:doctime: 17:31:54
:author: lingwen
:place: Vancouver
:category: tools
:tags: latex, beamer, slide
:stem: latexmath

I have 4 beautiful simple beamer templates to share, they are all compatible with English and Chinese.
You don't have to install any other fonts or softwares in order to compile them.
All you need is just a `make` or `xelatex`.
The code and pdfs could be found from https://github.com/synckey/beamers[github].

== algiers

https://raw.githubusercontent.com/synckey/beamers/master/algiers/algiers.pdf[pdf]

image::../static/images/beamer/algiers01.jpeg[]
image::../static/images/beamer/algiers02.jpeg[]
image::../static/images/beamer/algiers03.jpeg[]
image::../static/images/beamer/algiers04.jpeg[]
image::../static/images/beamer/algiers05.jpeg[]
image::../static/images/beamer/algiers06.jpeg[]
image::../static/images/beamer/algiers07.jpeg[]
image::../static/images/beamer/algiers08.jpeg[]
image::../static/images/beamer/algiers09.jpeg[]
image::../static/images/beamer/algiers10.jpeg[]
image::../static/images/beamer/algiers11.jpeg[]
image::../static/images/beamer/algiers12.jpeg[]
image::../static/images/beamer/algiers13.jpeg[]
image::../static/images/beamer/algiers14.jpeg[]

== iqss

https://raw.githubusercontent.com/synckey/beamers/master/iqss/iqss.pdf[pdf]

image::../static/images/beamer/iqss00.jpeg[]
image::../static/images/beamer/iqss01.jpeg[]
image::../static/images/beamer/iqss02.jpeg[]
image::../static/images/beamer/iqss03.jpeg[]
image::../static/images/beamer/iqss04.jpeg[]
image::../static/images/beamer/iqss05.jpeg[]
image::../static/images/beamer/iqss06.jpeg[]
image::../static/images/beamer/iqss07.jpeg[]
image::../static/images/beamer/iqss08.jpeg[]
image::../static/images/beamer/iqss09.jpeg[]
image::../static/images/beamer/iqss10.jpeg[]
image::../static/images/beamer/iqss11.jpeg[]
image::../static/images/beamer/iqss12.jpeg[]
image::../static/images/beamer/iqss13.jpeg[]
image::../static/images/beamer/iqss14.jpeg[]
image::../static/images/beamer/iqss15.jpeg[]
image::../static/images/beamer/iqss16.jpeg[]
image::../static/images/beamer/iqss17.jpeg[]
image::../static/images/beamer/iqss18.jpeg[]
image::../static/images/beamer/iqss19.jpeg[]
image::../static/images/beamer/iqss20.jpeg[]

=== 3. naked

https://raw.githubusercontent.com/synckey/beamers/master/naked/naked.pdf[pdf]

image::../static/images/beamer/naked00.jpeg[]
image::../static/images/beamer/naked01.jpeg[]
image::../static/images/beamer/naked02.jpeg[]
image::../static/images/beamer/naked03.jpeg[]
image::../static/images/beamer/naked04.jpeg[]
image::../static/images/beamer/naked05.jpeg[]
image::../static/images/beamer/naked06.jpeg[]
image::../static/images/beamer/naked07.jpeg[]
image::../static/images/beamer/naked08.jpeg[]
image::../static/images/beamer/naked09.jpeg[]
image::../static/images/beamer/naked10.jpeg[]
== 4. simple

https://raw.githubusercontent.com/synckey/beamers/master/simple/simple.pdf[pdf]

image::../static/images/beamer/simple00.jpeg[]
image::../static/images/beamer/simple01.jpeg[]
image::../static/images/beamer/simple02.jpeg[]
image::../static/images/beamer/simple03.jpeg[]
image::../static/images/beamer/simple04.jpeg[]
image::../static/images/beamer/simple05.jpeg[]
image::../static/images/beamer/simple06.jpeg[]
image::../static/images/beamer/simple07.jpeg[]
image::../static/images/beamer/simple08.jpeg[]